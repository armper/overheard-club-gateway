import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';

import { OverheardClubGatewaySharedModule } from 'app/shared/shared.module';
import { HOME_ROUTE, POSTS_BY_TOPIC } from './home.route';
import { HomeComponent } from './home.component';

@NgModule({
  imports: [OverheardClubGatewaySharedModule, RouterModule.forChild([HOME_ROUTE, POSTS_BY_TOPIC])],
  declarations: [HomeComponent]
})
export class OverheardClubGatewayHomeModule {}
