import { Route } from '@angular/router';

import { HomeComponent } from './home.component';
import { PostsByTopicComponent } from 'app/posts-by-topic/posts-by-topic.component';

export const HOME_ROUTE: Route = {
  path: '',
  component: HomeComponent,
  data: {
    authorities: [],
    pageTitle: 'home.title'
  }
};

export const POSTS_BY_TOPIC = {
  path: 'posts-by-topic/:id',
  component: PostsByTopicComponent
};
