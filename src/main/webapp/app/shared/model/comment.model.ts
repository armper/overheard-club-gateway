import { IPost } from 'app/shared/model/post.model';

export interface IComment {
  id?: string;
  content?: string;
  post?: IPost;
}

export class Comment implements IComment {
  constructor(public id?: string, public content?: string, public post?: IPost) {}
}
