import { IPost } from 'app/shared/model/post.model';

export interface ITopic {
  id?: string;
  title?: string;
  posts?: IPost[];
}

export class Topic implements ITopic {
  constructor(public id?: string, public title?: string, public posts?: IPost[]) {}
}
