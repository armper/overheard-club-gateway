import { IComment } from 'app/shared/model/comment.model';
import { ITopic } from 'app/shared/model/topic.model';

export interface IPost {
  id?: string;
  title?: string;
  content?: string;
  comments?: IComment[];
  topic?: ITopic;
}

export class Post implements IPost {
  constructor(public id?: string, public title?: string, public content?: string, public comments?: IComment[], public topic?: ITopic) {}
}
