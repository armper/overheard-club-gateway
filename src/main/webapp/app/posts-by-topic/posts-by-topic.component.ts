import { Component, OnInit, Input } from '@angular/core';
import { ITopic } from 'app/shared/model/topic.model';
import { ActivatedRoute } from '@angular/router';
import { TopicService } from 'app/entities/topic/topic.service';
import { HttpResponse } from '@angular/common/http';
import { IPost } from 'app/shared/model/post.model';
import { PostService } from 'app/entities/post/post.service';
import { map, filter } from 'rxjs/operators';

@Component({
  selector: 'jhi-posts-by-topic',
  templateUrl: './posts-by-topic.component.html',
  styleUrls: ['./posts-by-topic.component.scss']
})
export class PostsByTopicComponent implements OnInit {
  @Input()
  topic!: ITopic;

  posts!: IPost[];

  constructor(private route: ActivatedRoute, private topicService: TopicService, private postService: PostService) {}

  ngOnInit(): void {
    this.getTopic();

    this.getPosts();
  }

  getTopic(): void {
    this.topicService.find(this.route.snapshot.paramMap.get('id') + '').subscribe((res: HttpResponse<ITopic>) => (this.topic = res.body!));
  }

  getPosts(): void {
    this.postService
      .query()
      .pipe(map(response => response.body!.filter(post => post.topic!.id === this.topic.id)))
      .subscribe(posts => (this.posts = posts));
  }
}
