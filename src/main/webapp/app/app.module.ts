import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';

import './vendor';
import { OverheardClubGatewaySharedModule } from 'app/shared/shared.module';
import { OverheardClubGatewayCoreModule } from 'app/core/core.module';
import { OverheardClubGatewayAppRoutingModule } from './app-routing.module';
import { OverheardClubGatewayHomeModule } from './home/home.module';
import { OverheardClubGatewayEntityModule } from './entities/entity.module';
// jhipster-needle-angular-add-module-import JHipster will add new module here
import { MainComponent } from './layouts/main/main.component';
import { NavbarComponent } from './layouts/navbar/navbar.component';
import { FooterComponent } from './layouts/footer/footer.component';
import { PageRibbonComponent } from './layouts/profiles/page-ribbon.component';
import { ActiveMenuDirective } from './layouts/navbar/active-menu.directive';
import { ErrorComponent } from './layouts/error/error.component';
import { PostsByTopicComponent } from './posts-by-topic/posts-by-topic.component';

@NgModule({
  imports: [
    BrowserModule,
    OverheardClubGatewaySharedModule,
    OverheardClubGatewayCoreModule,
    OverheardClubGatewayHomeModule,
    // jhipster-needle-angular-add-module JHipster will add new module here
    OverheardClubGatewayEntityModule,
    OverheardClubGatewayAppRoutingModule
  ],
  declarations: [
    MainComponent,
    NavbarComponent,
    ErrorComponent,
    PageRibbonComponent,
    ActiveMenuDirective,
    FooterComponent,
    PostsByTopicComponent
  ],
  bootstrap: [MainComponent]
})
export class OverheardClubGatewayAppModule {}
