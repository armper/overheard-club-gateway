/**
 * Data Access Objects used by WebSocket services.
 */
package com.panda.overheard.web.websocket.dto;
